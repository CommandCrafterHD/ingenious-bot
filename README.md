# Ingenious Bot
A simple discord bot

## Normal Commands
|**Command**|**Description**|
|-----------|---------------|
|[help](/modules/help.py)|Shows you all commands or specific command infos|
|[embed](/modules/embed.py)|Embeds a message for you|
|[dice](/modules/dice.py)|Throws a die with 6 sides|
|[trivia](/modules/trivia.py)|Asks you a multiple choice question|
|[poll](/modules/poll.py)|Lets you create simple and quick polls|

## Admin Commands
|**Command**|**Description**|
|-----------|---------------|
|[prefix](/modules/prefix.py)|Lets you change the server specific prefix|
|[modules](/modules/modules.py)|Lets you enable/disable certain modules|

## Dev Commands
|**Command**|**Description**|
|-----------|---------------|
|[eval](/modules/eval.py)|Evals python expressions|
|[halt](/modules/halt.py)|Halts all of the bots activity|
|[resume](/modules/halt.py)|Resumes all of the bots activity|

## Config example
Create a folder called **configs** if it doesnt exist yet, and put
a file inside called **main.json**, with the following code inside:
```json
{
   "token": "YOUR_TOKEN_HERE!",
   "default_prefix": "YOUR_PREFIX_HERE!",
   "devs": ["DEV", "ID's", "HERE!"],
   "client_id": "YOUR_BOTS_ID_HERE!",
   "client_secret": "YOUR_BOTS_SECRET_HERE!",
   "session_secret_key": "RANDOM_STRING_HERE!"
}
```
### Config-Value explanations

#### Token
The token is like the username and password your bot uses to log on, you can find it [here](https://www.discordapp.com/developers/applications)

#### Default prefix
The prefix is what the bot listens to, to know what commands are meant for it.
For example if you set it to `!` the bot will listen to all commands starting with `!`
e.g. `!help`

#### Devs
This is the list of ID's that have full right to use all commands and functions of this bot.
*!!CAUTION!!* Only add people here that you *REALLY* trust! You can mess stuff up with these rights!

#### Client id (Only for the web-panel)
This is the ID of your bot's account, wich you can get [here](https://www.discordapp.com/developers/applications)

#### Client secret (Only for the web-panel)
This is the secret of your bot's account, wich you can get [here](https://www.discordapp.com/developers/applications) (Its like a password, so dont share it anywhere!)

#### Session secret key (Only for the web-panel)
This value just needs to be secure, you dont need to remember this wich means you could a handy generator like [this one](https://www.randomlists.com/string)

## Installing the requirements
This should work by running this command:
```bash
pip3.6 install -r requirements.txt
```

## Made by
+ @CommandCrafterHD
+ @romangraef