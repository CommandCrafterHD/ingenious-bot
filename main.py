import json
import sqlite3
import traceback
from asyncio import coroutine

import discord
from pyfiglet import Figlet

import module_loader
from files import colors
from files import shared

token = ""
default_prefix = ""
devs = []

pref_conn = sqlite3.connect('configs/prefixes.db')
module_conn = sqlite3.connect('configs/modules.db')


def load_configs():
    try:
        file = json.loads(open("configs/main.json", "r").read())
    except Exception as e:
        print(colors.RED)
        print(e)
        print(colors.END)
        quit(404)
    global token, default_prefix, devs
    token = file["token"]
    default_prefix = file["default_prefix"]
    devs = file["devs"]
    print("-" * 40)
    print(f"{colors.MAGENTA}Token{colors.END}: {colors.CYAN}{token}{colors.END}")
    print(f"{colors.MAGENTA}Default prefix:{colors.END}: {colors.CYAN}{default_prefix}{colors.END}")
    dev_list = ""
    if len(devs) > 0:
        for i in devs:
            dev_list += f"\n - {colors.CYAN}{i}{colors.END}"
    else:
        dev_list = colors.YELLOW + "\nINFO: THERE ARE CURRENTLY NO DEVS SET, THIS WILL LIMIT THE BOTS USABILITY!"
    print(f"{colors.MAGENTA}Devs:{colors.END} {dev_list}")


if __name__ == "__main__":
    load_configs()




class BotClient(discord.Client):
    async def on_ready(self):
        print("-" * 40)
        print(f"{colors.MAGENTA}Client-Name{colors.END}: {colors.CYAN}{client.user.name}{colors.END}")
        print(f"{colors.MAGENTA}Client-ID{colors.END}: {colors.CYAN}{str(client.user.id)}{colors.END}")
        print("-" * 40)
        module_loader.load_package('modules')
        self.register_handlers()
        print("-" * 40)
        print(colors.CYAN + Figlet(font="slant").renderText("Ingenious-Bot").rstrip() + colors.END)

    async def on_message(self, message):
        if message.author == client.user:
            return
        if isinstance(message.channel, discord.abc.PrivateChannel) and not shared.halted:
            await message.author.send("Please don't send private messages.")
            return
        try:
            c = pref_conn.cursor()
            c.execute("SELECT * FROM prefixes WHERE id = ?;", (str(message.channel.guild.id),))
            prefix_to_use = c.fetchone()[1]
        except:
            prefix_to_use = default_prefix
        if not message.content.startswith(prefix_to_use):
            return
        cmd = message.content.split(' ')[0][len(prefix_to_use):].lower()
        if shared.halted and cmd not in ["resume", "halt"]:
            return
        try:
            c = module_conn.cursor()
            c.execute("SELECT * FROM modules WHERE sid = ?;", (str(message.channel.guild.id),))
            disabled_cmds = []
            for i in c.fetchall():
                disabled_cmds.append(i[1])
            if cmd.lower() in disabled_cmds:
                return
        except:
            pass
        if cmd in module_loader.commands:
            command = module_loader.commands[cmd]
            is_dev = str(message.author.id) in devs
            is_admin = message.author.guild_permissions.administrator
            if command.admin and not is_admin:
                if not is_dev:
                    await message.channel.send(
                        embed=discord.Embed(title="Error",
                                            description="You need to be a server administrator to run this command!",
                                            color=discord.Color.red()))
                    return
            if command.dev and not is_dev:
                await message.channel.send(
                    embed=discord.Embed(title="Error",
                                        description="You need to be a bot developer to run this command!",
                                        color=discord.Color.red()))
                return
            await command.func(module_loader.CommandContext(client,
                                                            message,
                                                            prefix_to_use,
                                                            command.category,
                                                            command.description,
                                                            is_dev,
                                                            is_admin,
                                                            command.syntax))
        else:
            await message.channel.send("Unknown command: " + cmd)

    def register_handlers(self):
        for ev, handlers in module_loader.events.items():
            old_handler = getattr(self, ev, coroutine(lambda *args, **kwargs: None))

            async def handler(*args, **kwargs):
                for h in [old_handler, *handlers]:
                    try:
                        await h(*args, **kwargs)
                    except:
                        traceback.print_exc()

            handler.__name__ = ev
            self.event(handler)


if __name__ == "__main__":
    client = BotClient()
    client.run(token)
