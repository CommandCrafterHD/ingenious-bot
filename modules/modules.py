import module_loader
import discord
import sqlite3

conn = sqlite3.connect('configs/modules.db')

try:
    c = conn.cursor()
    c.execute("SELECT * FROM modules;")
except sqlite3.OperationalError:
    c = conn.cursor()
    c.execute("CREATE TABLE modules (sid varchar(255), module varchar(255));")
    conn.commit()


@module_loader.bot_command("modules", "Admin", "Lets you disable/enable certain modules!", ["enable <module>", "disable <module>", "list"], admin=True)
async def modules_cmd(ctx: module_loader.CommandContext):
    cmds = []
    locked_cmds = ["eval", "help", "modules", "prefix", "halt", "resume"]
    global c

    disabled_cmds = []
    c.execute("SELECT * FROM modules WHERE sid = ?;", (ctx.discord.channel.guild.id,))
    for i in c.fetchall():
        disabled_cmds.append(i[1])

    for i in module_loader.commands.keys():
        if i in locked_cmds:
            continue
        cmds.append(i.lower())
    if len(ctx.args) > 0:
        if ctx.args[1].lower() == "enable":
            if len(ctx.args) > 1:
                if ctx.args[2].lower() in locked_cmds:
                    await ctx.fail(description="This command CANNOT be disabled/enabled!")
                    return
                elif ctx.args[2].lower() in cmds:
                    if ctx.args[2].lower() in disabled_cmds:
                        c.execute("DELETE FROM modules WHERE sid = ? AND module = ?;", (ctx.discord.channel.guild.id, ctx.args[2].lower()))
                        conn.commit()
                        await ctx.succeed(description=f"Module **{ctx.args[2]}** enabled!")
                    else:
                        await ctx.fail("This command is not disabled!")
                        return
                else:
                    await ctx.fail(description=f"Command **{ctx.args[2].lower()}** not found! Use **{ctx.prefix}modules list** for a list of commands!")
                    return
            else:
                await ctx.fail(description=f"You need to specify which module to enable! Use **{ctx.prefix}modules list** to see wich modules are disabled!")
                return
        elif ctx.args[1].lower() == "disable":
            if len(ctx.args) > 1:
                if ctx.args[2].lower() in locked_cmds:
                    await ctx.fail(description="This command CANNOT be disabled/enabled!")
                    return
                if ctx.args[2].lower() in cmds:
                    if ctx.args[2].lower() in disabled_cmds:
                        await ctx.fail(description="This command is not enabled!")
                        return
                    else:
                        c.execute("INSERT INTO modules (sid, module) VALUES (?,?);", (ctx.discord.channel.guild.id, ctx.args[2].lower()))
                        conn.commit()
                        await ctx.succeed(description=f"Module **{ctx.args[2]}** disabled!")
                else:
                    await ctx.fail(description=f"Command **{ctx.args[2].lower()}** not found! Use **{ctx.prefix}modules list** for a list of commands!")
                    return
            else:
                await ctx.fail(description=f"You need to specify which module to enable! Use **{ctx.prefix}modules list** to see wich modules are disabled!")
                return
        elif ctx.args[1].lower() == "list":
            em = discord.Embed(color=discord.Colour.blurple(), title="Modules", description="Here's a list of modules, :white_check_mark: means its enabled, :x: means its disabled and :no_entry_sign: means it cant be disabled!")
            commands = ""
            for i in module_loader.commands.keys():
                if i in disabled_cmds:
                    commands += i + " | :x:\n"
                if i in cmds:
                    commands += i + " | :white_check_mark:\n"
                if i in locked_cmds:
                    commands += i + " | :no_entry_sign:\n"
            em.add_field(name="Modules", value=commands)
            await ctx.discord.channel.send(embed=em)
        else:
            await ctx.fail(description=f"Option **{ctx.args[1]}** not found! Use **{ctx.prefix}help modules** for more info!")
            return
    else:
        await ctx.fail(description=f"You need to specify what to do! Use **{ctx.prefix}help modules** for more info!")
        return
