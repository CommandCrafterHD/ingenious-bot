import module_loader
import random
from discord import Embed, Colour


@module_loader.bot_command("dice", "Fun", "Rolls a die with 6 sides for you", [])
async def dice_cmd(ctx: module_loader.CommandContext):
    thrown = random.choice([":one:", ":two:", ":three:", ":four:", ":five:", ":six:"])
    await ctx.discord.channel.send(embed=Embed(color=Colour.blue(), title="Dice", description=f"You rolled a {thrown}!"))
