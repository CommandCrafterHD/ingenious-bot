import module_loader
import discord


@module_loader.bot_command("poll", "Uncategorized", """
To create a title for the poll end the first argument with a colon or question mark (title: / title?)
You can use whitespaces in title/options, by surrounding them with double quotes ("some text")
If you want to use quotes in your text, you have to put a backslash in front (\\\\")
If you want to use backslashes in your text, you have to put another one in front (\\\\\\\\)""", ["option1 option2 ... optionN"])
async def poll_cmd(ctx: module_loader.CommandContext):
    options = ctx.args
    if len(options) == 1:
        await ctx.fail("Needs options to choose from!")
        return

    options = options[1:]

    if len(options) > 20:
        await ctx.fail("Can't have more than 20 options. :(")
        return

    em = discord.Embed(color=discord.Color.blue(), description=ctx.discord.author.mention + " has started a poll:")
    em.set_author(name=ctx.discord.author.display_name, icon_url=ctx.discord.author.avatar_url)

    msg = ""

    if options[0][-1] in [':', '?']:
        if len(options) == 1:
            await ctx.fail("Needs options to choose from!")
            return
        msg += '\n' + options[0]
        options = options[1:]

    for i in range(len(options)):
        msg += "\n" + chr(i + 65) + ": " + options[i]

    em.description += msg

    sent_msg = await ctx.discord.message.channel.send(embed=em)

    for i in range(len(options)):
        await sent_msg.add_reaction(chr(0x1F1E6 + i))
