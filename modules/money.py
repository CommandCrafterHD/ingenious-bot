import module_loader
import sqlite3
import time
import asyncio

cooldown = 60  # The interval between given money in seconds
money_to_give = 10  # The amount of money a user gets per message
start_credit = 100  # The amount of money new users get

conn = sqlite3.connect('configs/wallets.db')

cmd_cooldown = 30  # The interval between given money in seconds
cmd_cooldown_dict = {}

try:
    c = conn.cursor()
    c.execute("SELECT COUNT(1) FROM wallets WHERE 1 = 0")
except sqlite3.OperationalError:
    c = conn.cursor()
    c.execute("CREATE TABLE wallets (uid BIGINT, sid BIGINT, money varchar(255), lastmsg varchar(255));")
    conn.commit()


def add_user(uid, sid):
    c = conn.cursor()
    c.execute("INSERT INTO wallets (uid, sid, money, lastmsg) VALUES (?, ?, ?, ?);", (int(uid), int(sid), str(start_credit), str(int(time.time()))))
    conn.commit()
    c.execute("SELECT * FROM wallets WHERE uid = ? AND sid = ?;", (uid, sid))
    return c.fetchone()


def get_user(uid, sid):
    c = conn.cursor()
    c.execute("SELECT * FROM wallets WHERE uid = ? AND sid = ?;", (uid, sid))
    user = c.fetchone()
    if not user:
        return add_user(uid, sid)
    else:
        return user


def add_money(uid, sid, amount, lastmsg):
    usr = get_user(uid, sid)
    c = conn.cursor()
    if not lastmsg:
        c.execute("UPDATE wallets SET money = ? WHERE uid = ? AND sid = ?;", (str(float(usr[2]) + amount), uid, sid))
    else:
        c.execute("UPDATE wallets SET money = ?, lastmsg = ? WHERE uid = ? AND sid = ?;", (str(float(usr[2]) + amount), str(lastmsg), uid, sid))
    conn.commit()
    return get_user(uid, sid)


def remove_money(uid, sid, amount, lastmsg):
    usr = get_user(uid, sid)
    c = conn.cursor()
    if not lastmsg:
        c.execute("UPDATE wallets SET money = ? WHERE uid = ? AND sid = ?;", (str(float(usr[2]) - amount), uid, sid))
    else:
        c.execute("UPDATE wallets SET money = ?, lastmsg = ? WHERE uid = ? AND sid = ?;", (str(float(usr[2]) - amount), str(lastmsg), uid, sid))
    conn.commit()
    return get_user(uid, sid)


@module_loader.bot_event()
async def on_message(message):
    user = get_user(message.author.id, message.channel.guild.id)
    if int(time.time()) - int(user[3]) > cooldown:
        add_money(message.author.id, message.channel.guild.id, money_to_give, int(time.time()))


@module_loader.bot_command("wallet", "Wallet", "Shows you how much money you or others currently have!", ["", "<user-mention>"])
async def wallet_cmd(ctx: module_loader.CommandContext):
    if len(ctx.discord.message.mentions) > 0:
        usr = get_user(ctx.discord.message.mentions[0].id, ctx.discord.message.guild.id)
        await ctx.succeed(title="Your wallet", description=f"{ctx.discord.message.mentions[0].mention} currently has {usr[2]}$ in his/her wallet!")
    else:
        usr = get_user(ctx.discord.message.author.id, ctx.discord.message.guild.id)
        await ctx.succeed(title="Your wallet", description=f"You currently have {usr[2]}$ in your wallet!")


@module_loader.bot_command("wadd", "Wallet", "Adds X amount of money to someones wallet! (Admins only)", ["<user-mention> <amount>"], admin=True)
async def wallet_cmd(ctx: module_loader.CommandContext):
    if len(ctx.args) > 2:
        if len(ctx.discord.message.mentions) > 0:
            add_money(ctx.discord.message.author.id, ctx.discord.message.channel.guild.id, float(ctx.args[2]), int(time.time()))
            usr_new = get_user(ctx.discord.message.author.id, ctx.discord.message.guild.id)
            await ctx.succeed(title="Wallet updated", description=f"{ctx.discord.message.mentions[0].name} has now got {usr_new[2]}$ in their wallet!")
        else:
            await ctx.fail(description="You need to mention who to give the money!")
    else:
        await ctx.fail(description="You need to specify who to give money to, and how much money to give!")


@module_loader.bot_command("wremove", "Wallet", "Removes X amount of money from someones wallet! (Admins only)", ["<user-mention> <amount>"], admin=True)
async def wallet_cmd(ctx: module_loader.CommandContext):
    if len(ctx.args) > 2:
        if len(ctx.discord.message.mentions) > 0:
            remove_money(ctx.discord.message.author.id, ctx.discord.message.channel.guild.id, float(ctx.args[2]), int(time.time()))
            usr_new = get_user(ctx.discord.message.author.id, ctx.discord.message.guild.id)
            await ctx.succeed(title="Wallet updated", description=f"{ctx.discord.message.mentions[0].name} has now got {usr_new[2]}$ in their wallet!")
        else:
            await ctx.fail(description="You need to mention who to remove the money from!")
    else:
        await ctx.fail(description="You need to specify who to remove the money from, and how much money to remove!")


@module_loader.bot_command("wgive", "Wallet", "Gives another user X amount of your money!", ["<user-mention> <amount>"])
async def wallet_cmd(ctx: module_loader.CommandContext):

    if ctx.discord.author.id not in cmd_cooldown_dict.keys():
        cmd_cooldown_dict[ctx.discord.author.id] = int(time.time())
        pass
    else:
        if int(time.time()) - int(cmd_cooldown_dict[ctx.discord.author.id]) >= cmd_cooldown:
            cmd_cooldown_dict.pop(ctx.discord.author.id)
            pass
        else:
            return

    def check(reaction, user):
        if reaction.emoji in ["✅", "❌"] and user.id == ctx.discord.author.id:
            return reaction

    if len(ctx.args) > 2:
        if len(ctx.discord.message.mentions) > 0:
            usr = get_user(ctx.discord.message.author.id, ctx.discord.message.guild.id)
            if float(usr[2]) < float(ctx.args[2]):
                await ctx.fail(title="Wallet", description="You do not have enough money for this action!")
            else:
                msg = await ctx.succeed(title="Wallet", description=f"Do you really want to give {ctx.discord.message.mentions[0].mention} {ctx.args[2]}$?")
                await msg.add_reaction("✅")
                await msg.add_reaction("❌")
                try:
                    reaction = await ctx.discord.client.wait_for('reaction_add', timeout=30.0, check=check)
                except asyncio.TimeoutError:
                    await msg.delete()
                    await ctx.fail(title="Wallet", description="Timed out! You did not confirm the transaction!")
                else:
                    if reaction[0].emoji == "✅":
                        print(str(get_user(ctx.discord.message.author.id, ctx.discord.message.guild.id)[2]) + " " + str(ctx.args[2]))
                        if float(get_user(ctx.discord.message.author.id, ctx.discord.message.guild.id)[2]) >= float(ctx.args[2]):
                            await msg.delete()
                            await ctx.succeed(title="Wallet", description="Transaction confirmed!")
                            remove_money(ctx.discord.message.author.id, ctx.discord.message.guild.id, float(ctx.args[2]), None)
                            add_money(ctx.discord.message.mentions[0].id, ctx.discord.message.guild.id, float(ctx.args[2]), None)
                        else:
                            await ctx.fail(title="Error", description="You do not have enough money to perform this action!")
                    elif reaction[0].emoji == "❌":
                        await msg.delete()
                        await ctx.succeed(title="Wallet", description="Transaction canceled!")
                        if ctx.discord.author.id in cmd_cooldown_dict.keys():
                            cmd_cooldown_dict.pop(ctx.discord.author.id)
        else:
            await ctx.fail(description="You need to mention who to give the money!")
    else:
        await ctx.fail(description="You need to specify who to give the money, and how much money to give!")

