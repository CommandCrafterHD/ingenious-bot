import module_loader
import discord


@module_loader.bot_command("help", "Uncategorized", "Shows you all commands!", ["", "<command>"])
async def help_cmd(ctx: module_loader.CommandContext):
    if len(ctx.args) > 1:
        cmd = ctx.args[1].lower()
        if cmd.startswith(ctx.prefix):
            cmd = cmd[len(ctx.prefix):]
        if cmd in module_loader.commands.keys():
            em = discord.Embed(color=discord.Colour.blue(), title=f"Command info - {cmd.title()}", description="")
            em.add_field(name="Description", value=module_loader.commands[cmd].description)
            if not module_loader.commands[cmd].syntax:
                syntax = f"{ctx.prefix}{cmd}\n"
            else:
                syntax = ""
            for i in module_loader.commands[cmd].syntax:
                syntax += f"{ctx.prefix}{cmd} {i}\n"
            em.add_field(name="Syntax", value=syntax, inline=False)
            await ctx.discord.channel.send(embed=em)
        else:
            await ctx.fail(description=f"Command **{cmd}** could not be found!")
    else:
        categorized = {}
        categorized_dev = ""
        categorized_admin = ""
        for i in module_loader.commands.values():
            if i.category.lower() == "dev":
                categorized_dev += ctx.prefix + i.cmd + "\n"
                continue
            if i.category.lower() == "admin":
                categorized_admin += ctx.prefix + i.cmd + "\n"
                continue
            try:
                categorized[i.category] += ctx.prefix + i.cmd + "\n"
            except KeyError:
                categorized[i.category] = ctx.prefix + i.cmd + "\n"
        em = discord.Embed(color=discord.Colour.blue(), title="All commands", description=f"To see detailed info on commands, use **{ctx.prefix}help <command>**!\n")
        em2 = discord.Embed(color=discord.Colour.red(), title="Dev commands", description=f"To see detailed info on commands, use **{ctx.prefix}help <command>**!\n")
        em3 = discord.Embed(color=discord.Colour.gold(), title="Admin commands",description=f"To see detailed info on commands, use **{ctx.prefix}help <command>**!\n")
        for i in categorized.keys():
            em.add_field(name=i, value=categorized[i], inline=False)
        embeds = [em]
        em2.add_field(name="Dev", value=categorized_dev)
        em3.add_field(name="Admin", value=categorized_admin)
        if em3.description:
            embeds.append(em3)
        if em2.description:
            embeds.append(em2)
        for i in embeds:
            await ctx.discord.channel.send(embed=i)
