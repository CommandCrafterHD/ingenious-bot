#  https://opentdb.com/api.php?amount=1&difficulty=easy&type=multiple
import module_loader
import discord
import random
import asyncio
import requests
import html


def get_question(difficulty: str):
    link = f"https://opentdb.com/api.php?amount=1&difficulty={difficulty}&type=multiple"
    request = requests.get(link).json()["results"][0]
    answers = [
        (request["correct_answer"], True)
    ]
    for i in request["incorrect_answers"]:
        answers.append((i, False))
    answers_shuffled = answers[:]
    random.shuffle(answers_shuffled)
    question = html.unescape(request["question"])
    return {
        "category": request["category"],
        "difficulty": request["difficulty"],
        "question": question,
        "answers": answers_shuffled,
        "correct_answer": request["correct_answer"]
    }


@module_loader.bot_command("trivia", "Fun", "Asks you a trivia question, with either a given difficulty or just \"easy\" wich is the default", ["", "<easy/medium/hard>"])
async def trivia_cmd(ctx: module_loader.CommandContext):
    difficulty = "easy"
    if len(ctx.args) > 1:
        if ctx.args[1].lower() in ["easy", "medium", "hard"]:
            if ctx.args[1].lower() == "easy":
                difficulty = "easy"
            elif ctx.args[1].lower() == "medium":
                difficulty = "medium"
            else:
                difficulty = "hard"
        else:
            await ctx.fail(description=f"Difficulty **{ctx.args[1]}** not found! Use **easy / medium / hard**!")
            return
    question = get_question(difficulty)
    if difficulty == "easy":
        color = discord.Colour.green()
    elif difficulty == "medium":
        color = discord.Colour.gold()
    else:
        color = discord.Colour.red()
    em = discord.Embed(title="Trivia", description="", color=color)
    em.set_author(name="API by opentdb.com", url="https://opentdb.com")
    em.add_field(name="Category", value=question["category"])
    em.add_field(name="Difficulty", value=question["difficulty"].title(), inline=True)
    em.add_field(name="Question", value=question["question"], inline=False)
    answers = ""
    letters = [(":regional_indicator_a:", "🇦"), (":regional_indicator_b:", "🇧"), (":regional_indicator_c:", "🇨"), (":regional_indicator_d:", "🇩")]
    for i in question["answers"]:
        answers += letters[question["answers"].index(i)][0] + " - " + i[0] + "\n"
    em.add_field(name="Answers", value=answers)
    msg = await ctx.discord.channel.send(embed=em)
    for i in question["answers"]:
        await msg.add_reaction(letters[question["answers"].index(i)][1])

    def check(reaction, user):
        if reaction.emoji in ["🇦", "🇧", "🇨", "🇩"] and user.id == ctx.discord.author.id:
            return reaction

    try:
        reaction = await ctx.discord.client.wait_for('reaction_add', timeout=30.0, check=check)
    except asyncio.TimeoutError:
        em = discord.Embed(title="TIME's UP!", description="", color=discord.Colour.red())
        em.set_author(name="API by opentdb.com", url="https://opentdb.com")
        em.add_field(name="Category", value=question["category"])
        em.add_field(name="Difficulty", value=question["difficulty"].title(), inline=True)
        em.add_field(name="Question", value=question["question"], inline=False)
        em.add_field(name="Correct Answer", value="?")
        await msg.edit(embed=em)
        await msg.clear_reactions()
    else:
        react_index = ["🇦", "🇧", "🇨", "🇩"].index(reaction[0].emoji)
        answer = question["answers"][react_index]
        if answer[1]:
            em = discord.Embed(title="CORRECT!", description="", color=discord.Colour.green())
            em.set_author(name="API by opentdb.com", url="https://opentdb.com")
            em.add_field(name="Category", value=question["category"])
            em.add_field(name="Difficulty", value=question["difficulty"].title(), inline=True)
            em.add_field(name="Question", value=question["question"], inline=False)
            em.add_field(name="Correct Answer", value=["🇦", "🇧", "🇨", "🇩"][react_index] + " - " + question["correct_answer"])
            await msg.edit(embed=em)
            await msg.clear_reactions()
        else:
            em = discord.Embed(title="INCORRECT!", description="", color=discord.Colour.red())
            em.set_author(name="API by opentdb.com", url="https://opentdb.com")
            em.add_field(name="Category", value=question["category"])
            em.add_field(name="Difficulty", value=question["difficulty"].title(), inline=True)
            em.add_field(name="Question", value=question["question"], inline=False)
            em.add_field(name="Correct Answer", value=["🇦", "🇧", "🇨", "🇩"][react_index] + " - " + question["correct_answer"])
            await msg.edit(embed=em)
            await msg.clear_reactions()
