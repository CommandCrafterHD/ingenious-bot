import module_loader
from files import eval_handler


@module_loader.bot_command("eval", "Dev", "Evals a certain python expression", ["<code>"], dev=True)
async def eval_cmd(ctx: module_loader.CommandContext):
    args = ctx.discord.message.content.split(" ", 1)
    if len(args) != 2:
        return await ctx.fail(description="gimme code")
    await eval_handler.handle_eval(ctx.discord.message, ctx.discord.client, args[1])