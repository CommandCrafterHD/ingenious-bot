import module_loader
import discord


@module_loader.bot_command("embed", "Uncategorized", "Lets you embed stuff!\nFor color, you can either use one of the presets\n(**red; green; blue; orange; blurple; greyple; gold**) or a HEX code like #ffffff", ["<color> <title> <description>"])
async def embed_cmd(ctx: module_loader.CommandContext):
    try:
        color = ctx.args[1].lower()
        title = ctx.args[2]
        description = ctx.args[3]
    except IndexError:
        await ctx.fail("There are not enough arguments given!")
        return
    colors = {
        "red": discord.Colour.red(),
        "green": discord.Colour.green(),
        "blue": discord.Colour.blue(),
        "orange": discord.Colour.orange(),
        "blurple": discord.Colour.blurple(),
        "greyple": discord.Colour.greyple(),
        "gold": discord.Colour.gold()
    }
    if color.lower() in colors.keys():
        color = colors[color]
    elif color.lower().startswith("#"):
        try:
            color = int(color[1:], 16)
        except:
            await ctx.fail("Color was not found/could not get converted!")
            return
    else:
        try:
            color = int(color, 16)
        except:
            await ctx.fail("Color was not found/could not get converted!")
            return
    em = discord.Embed(color=color, title=title, description=description)
    em.set_author(icon_url=ctx.discord.author.avatar_url, name=ctx.discord.author.display_name)
    await ctx.discord.channel.send(embed=em)
    await ctx.discord.message.delete()
