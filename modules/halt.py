import module_loader
import os
from files import shared


def hostname():
    import socket
    return socket.gethostname()


@module_loader.bot_command("halt", "Dev", "Halts all current activities of the bot.", [], dev=True)
async def halt_cmd(ctx: module_loader.CommandContext):
    if shared.halted:
        await ctx.fail(description="The bot is already halted!")
    else:
        await ctx.succeed(description=f"Bot halted on {hostname()}")
        shared.halted = True


@module_loader.bot_command("resume", "Dev", "Resumes all current activities of the bot.", [], dev=True)
async def resume_cmd(ctx: module_loader.CommandContext):
    if shared.halted:
        shared.halted = False
        await ctx.succeed(description="Bot functionality resumed!")
    else:
        await ctx.fail(description="The bot is not yet halted!")
