import module_loader
import sqlite3

conn = sqlite3.connect('configs/prefixes.db')

try:
    c = conn.cursor()
    c.execute("SELECT * FROM prefixes;")
except sqlite3.OperationalError:
    c = conn.cursor()
    c.execute("CREATE TABLE prefixes (id varchar(255), prefix varchar(255));")
    conn.commit()


@module_loader.bot_command("prefix", "Admin", "Lets you set the prefix for this server!", ["<prefix>"], admin=True)
async def prefix_cmd(ctx: module_loader.CommandContext):
    if len(ctx.args) > 1:
        if len(ctx.args[1]) < 6:
            global c
            c.execute("SELECT * FROM prefixes WHERE id = ?", (str(ctx.discord.channel.guild.id),))
            if c.fetchone():
                c.execute("UPDATE prefixes SET prefix = ? WHERE id = ?", (ctx.args[1].lower(), str(ctx.discord.channel.guild.id)))
                conn.commit()
                await ctx.succeed(description=f"Prefix changed to: **{ctx.args[1].lower()}**!")
                return
            else:
                c.execute("INSERT INTO prefixes (id, prefix) VALUES (?, ?)", (str(ctx.discord.channel.guild.id), ctx.args[1].lower()))
                conn.commit()
                await ctx.succeed(description=f"Prefix changed to: **{ctx.args[1].lower()}**!")
                return
        else:
            await ctx.fail("You should not set a prefix with a length over 5!")
    else:
        await ctx.fail("You need to give a new prefix!")
