import json

import querystring
import requests
import requests.exceptions
from flask import Flask, redirect, render_template, session, request, url_for, send_file

from .assets import assets

SESSION_TYPE = 'memcache'
app = Flask(__name__)

config = json.load(open('configs/main.json'))

app.secret_key = config["session_secret_key"]

assets.init_app(app)

API_BASE_URL = 'https://discordapp.com/api/v6'


def get_discord_user(token):
    if token is None:
        return None
    resp = requests.get(API_BASE_URL + '/users/@me',
                        headers={
                            'Authorization': 'Bearer ' + token
                        })
    if 400 <= resp.status_code < 500:
        return None
    return resp.json()


def get_discord_id(token):
    user = get_discord_user(token)
    if user is not None:
        return user['id']
    return None


def get_avatar_url(user_dict):
    if user_dict is None:
        return 'none.png'
    return "https://cdn.discordapp.com/avatars/" + user_dict['id'] + '/' + user_dict['avatar'] + (
        '.gif' if user_dict['avatar'][0:2] == 'a_' else '.jpg')


def is_logged_in():
    return session.get('token') is not None and get_discord_user(session.get('token')) is not None


@app.context_processor
def api_processors():
    return dict(get_avatar_url=get_avatar_url,
                user=get_discord_user(session.get('token', None)),
                is_logged_in=is_logged_in)


def get_current_discord_user():
    return get_discord_user(session.get('token', None))


def get_guilds(token, refresh):
    try:
        if session['guilds'] and not refresh:
            return session['guilds']
    except KeyError:
        pass
    if token is None:
        return None
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + token
    }

    r = requests.get('https://discordapp.com/api/users/@me/guilds', headers=headers)
    session['guilds'] = r.json()
    return session['guilds']


def exchange_code(code):
    data = {
        'client_id': config['client_id'],
        'client_secret': config['client_secret'],
        'grant_type': 'authorization_code',
        'code': code,
        'redirect_uri': url_for('auth', _external=True),
        'scope': 'identify email connections'
    }
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    r = requests.post('https://discordapp.com/api/v6/oauth2/token', data=data, headers=headers)
    try:
        token = r.json()['access_token']
    except:
        token = None
    return token


@app.route('/')
def index():
    if is_logged_in():
        return redirect('/dashboard')
    else:
        return render_template('index.html')


@app.route('/logout')
def logout():
    session['token'] = None
    return redirect('/')


@app.route('/auth')
def auth():
    code = request.args.get('code')
    token = exchange_code(code)
    error = request.args.get('error')
    if error == "access_denied":
        return redirect('/')
    session['token'] = token
    return redirect('/')


@app.route('/dashboard')
def dashboard():
    if is_logged_in():
        user_dict = get_current_discord_user()
        return render_template('dashboard.html', values={
            'user': user_dict,
            'user_avatar': get_avatar_url(user_dict)
        }, guilds=get_guilds(session['token'], refresh=False))
    else:
        return redirect('/')


@app.route('/servers')
def servers():
    if is_logged_in():
        user_dict = get_current_discord_user()
        return render_template('servers.html', values={
            'user': user_dict,
            'user_avatar': get_avatar_url(user_dict)
        }, guilds=get_guilds(session['token'], refresh=False))
    else:
        return redirect('/')


# ERROR-HANDLER
def four_o_four(e):
    return render_template('404.html'), 404


def five_hundred(e):
    return render_template('500.html'), 500


app.register_error_handler(404, four_o_four)
app.register_error_handler(500, five_hundred)


# EASTEREGGS!
@app.route('/siegbert-schnoesel')
def siegbert():
    return send_file('static/Siegbert_Schnoesel.jpg')