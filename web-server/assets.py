from flask_assets import Environment, Bundle
from webassets.filter.requirejs import RequireJSFilter

assets = Environment()

scss = Bundle(
    'scss/main.scss',
    filters=['pyscss', 'cssmin'],
    output='gen/all.css'
)

home = Bundle(
    'scss/home.scss',
    filters=['pyscss', 'cssmin'],
    output='gen/home.css',
)

dashboard = Bundle(
    'scss/dashboard.scss',
    filters=['pyscss', 'cssmin'],
    output='gen/dashboard.css',
)

servers = Bundle(
    'scss/servers.scss',
    filters=['pyscss', 'cssmin'],
    output='gen/servers.css',
)

errorpage = Bundle(
    'scss/errorpage.scss',
    filters=['pyscss', 'cssmin'],
    output='gen/errorpage.css',
)

requirejs = RequireJSFilter()
requirejs.optimize = False
requirejs.config = 'js/requirejs.js'

ts = Bundle(
    'js/main.js',
    filters=[
        requirejs,
        'jsmin',
    ],
    output='gen/all.js',
)

assets.register('scss', scss)
assets.register('ts', ts)
assets.register('home', home)
assets.register('dashboard', dashboard)
assets.register('errorpage', errorpage)
