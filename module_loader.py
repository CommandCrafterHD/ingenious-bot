import importlib
import os
import shlex
from collections import defaultdict
from typing import List, Callable, Dict

import discord

from files import colors as color


def load_module(mod_name: str):
    try:
        importlib.import_module(mod_name)
        print("[ " + color.GREEN + "OK" + color.END + " ] Loaded module " + mod_name)
    except:
        print(
            "[" + color.RED + color.BOLDON + "FAIL" + color.BOLDOFF + color.END + "] Error loading module " + mod_name)


def load_package(package: str, file_suffix: str = ".py"):
    mod_dir = package.replace('.', '/')

    if os.path.isfile(mod_dir):
        load_module(package)

    for file in os.listdir(mod_dir):
        if not os.path.isfile(mod_dir + '/' + file):
            continue
        if not file.endswith(file_suffix):
            continue
        load_module(package + '.' + file[:-len(file_suffix)])


class CommandContext:
    def __init__(self,
                 discord_client: discord.Client,
                 discord_message: discord.Message,

                 prefix: str,
                 category: str,
                 description: str,
                 is_dev: bool,
                 is_admin: bool,
                 syntax: List[str]):
        self.discord = self.__DiscordData(discord_client, discord_message)
        self.prefix = prefix
        self.category = category
        self.description = description
        self.is_dev = is_dev
        self.is_admin = is_admin
        self.syntax = syntax
        self.message = discord_message.content
        self.message_parts = self.message.split(' ')
        try:
            self.args = shlex.split(self.message)
        except ValueError as e:
            if str(e) == "No closing quotation":
                self.args = shlex.split(self.message + '"')
        self.send = self.__send_send
        self.succeed = self.__send_success
        self.fail = self.__send_failure

    async def __send_send(self, message: str) -> discord.Message:
        return await self.discord.channel.send(message)

    async def __send_success(self, title: str = "Sucess!", description: str = None) -> discord.Message:
        if description is None:
            raise TypeError("Function Send-Sucess needs a given description!")
        embed = discord.Embed(title=title, description=description, color=discord.Colour.green())
        return await self.discord.channel.send(embed=embed)

    async def __send_failure(self, description: str, title: str = "Error") -> discord.Message:
        embed = discord.Embed(title=title, description=description, color=discord.Colour.red())
        return await self.discord.channel.send(embed=embed)

    class __DiscordData:
        def __init__(self,
                     client: discord.Client,
                     message: discord.Message):
            self.client = client
            self.message = message
            self.channel: discord.TextChannel = message.channel
            self.author: discord.Member = message.author


class Command:
    def __init__(self,
                 cmd: str,
                 category: str,
                 admin: bool,
                 dev: bool,
                 description: str,
                 syntax: List[str],
                 func: Callable[[CommandContext], None]):
        self.cmd = cmd
        self.category = category
        self.admin = admin
        self.dev = dev
        self.description = description
        self.syntax = syntax

        self.func = func


commands: Dict[str, Command] = {}
events: Dict[str, List[Callable]] = defaultdict(list)


def bot_command(cmd: str, category: str, description: str, syntax=None, admin: bool = False,
                dev: bool = False):
    if syntax is None:
        syntax = []

    def decorator(func):
        global commands
        commands[cmd.lower()] = Command(cmd=cmd, category=category, description=description, syntax=syntax, admin=admin,
                                        dev=dev, func=func)
        return func

    return decorator


def bot_event(name: str = None):
    def decorator(func, name=name):
        if name is None:
            name = func.__name__
        events[name.lower()].append(func)
        return func

    return decorator
